<?php
error_reporting(E_ALL);
mb_internal_encoding("UTF-8");
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';


function loop($options = []){
    $curl = $options['curl'];
    $templates = \prFeed\models\Templates::query()
        ->where('status', '=', 0)
        ->take(10)
        ->get();

    if($templates === null  || count($templates) < 1){
        return false;
    }


    foreach ($templates as $template){
        $template->status = 1;
        $template->save();
    }

    foreach ($templates as $template){
        $id = $template->id;
        $url = 'http://service-products.templatemonster.com/api/v1/products/en/' . $id . '?';
        $responseItem = $curl->call($url);
        $jsonItem = json_decode($responseItem, true);
        $state = isset($jsonItem['state']) ? (int)$jsonItem['state'] : null;
        $type = isset($jsonItem['templateType']['typeId']) ? (int)$jsonItem['templateType']['typeId']: null;
        if($state === null || $type === null){
            continue;
        }
        if($state !== 1||$type === 63){
            \prFeed\models\Templates::query()
                ->where('id', '=', $id)
                ->delete();
            continue;
        }

        $descrip = isset($jsonItem['templatePreviewFBTitle']) ? $jsonItem['templatePreviewFBTitle'] : null;
        if($descrip!==null){
            $descrip=strip_tags($descrip);
            $descrip = str_replace("\r", "", $descrip);
            $descrip = str_replace("\n", "", $descrip);
        }
        $title = null;
        if (isset($jsonItem['templateTitle'])) {
            $title = $jsonItem['templateTitle'];
            if (mb_strlen($jsonItem['templateTitle']) >= 150) {
                $title = mb_substr($jsonItem['templateTitle'], 0, 147) . '...';
            }
        }
        $price = isset($jsonItem['price']) ? (float)$jsonItem['price']: null;
        $brand = isset($jsonItem['typeShortName']) ?$jsonItem['typeShortName'] : null;
        $link = "https://www.templatemonster.com/moto-cms-3-templates/" . $id . ".html";
        $groupImg = mb_substr($id, 0, 3) . '00';
        $img = 'https://s.tmimgcdn.com/scr/' . $groupImg . '/' . $id . '-big.jpg?build=15';
        $dir = 'images/' . $groupImg;
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $filename = $dir . '/' . $id . '-big.jpg';
        if (!file_exists($filename)||filesize($filename)===0) {
            $imgContent = $curl->call($img);
            file_put_contents($filename, $imgContent);
        }
        $filename_google=$dir . '/' .$id.'-google.jpg';
        if(!file_exists($filename_google)||filesize($filename_google)===0){
            $google_pic=new prFeed\utils\ImageGener($filename);
            $google_pic->createImage(300,300);
            $google_pic->save($filename_google);
        }
        $filename_facebook=$dir . '/' .$id.'-facebook.jpg';
        if(!file_exists($filename_facebook)||filesize($filename_facebook)===0){
            $facebook_pic=new prFeed\utils\ImageGener($filename);
            $facebook_pic->createImage(1200,630);
            $facebook_pic->save($filename_facebook);
        }
        $template->title = $title;
        $template->description = $descrip;
        $template->price = $price;
        $template->brand = $brand;
        $template->link = $link;
        $template->img_fb = $filename_facebook;
        $template->img_google = $filename_google;
        $template->status = 2;
        $template->save();
    }
    return true;
}


$curl = new \prFeed\utils\Curl();
if (!is_dir("images")) {
    mkdir('images', 0777, true);
}

echo '<pre>';


$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'productsFeed',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
//
//$file = file_get_contents('templates.json', true);
//if ($file === null) {
//    echo "File is not found";
//    die();
//}
//$json = json_decode($file,true);
//foreach ($json as $item) {
//    $data = isset($item['data']) ? $item['data'] : null;
//    if ($data === null) {
//        continue;
//    }
//    foreach ($data as $templ) {
//        $id = isset($templ['id']) ? $templ['id'] : null;
//        $template = \prFeed\models\Templates::query()
//            ->where('id', '=', $id)
//            ->first();
//
//        if($template!==null){
//            continue;
//        }
//        $template=new \prFeed\models\Templates();
//        $template->id=$id;
//        $template->save();
//
//    }
//
//}
//
//die();
$curl = new \prFeed\utils\Curl();
if (!is_dir("images")) {
    mkdir('images', 0777, true);
}


do{
    $run = loop([
        'curl' => $curl
    ]);
}while($run);

$templates = \prFeed\models\Templates::query()
    ->get();

if($templates === null  || count($templates) < 1){
    return false;
}
$csvFile_google=new \prFeed\utils\CsvFile("feed_google.csv");
$csvFile_fb=new \prFeed\utils\CsvFile("feed_fb.csv");
$csvFile_fb->openFile();
$csvFile_google->openFile();
$csvFile_google->addToFile(
    array(
        "id",
        "title",
        "description",
        "link",
        "image_link",
        "availability",
        "price",
        "brand",
        "condition",
    )
);
$csvFile_fb->addToFile(
    array(
        "id",
        "availability",
        "condition",
        "description",
        "image_link",
        "link",
        "title",
        "price",
        "brand"
    )
);
foreach($templates as $template){
    $values_google = array (
        $template->id,
        $template->title,
        $template->description,
        $template->link,
        $template->img_google,
        "in_stock",
        $template->price,
        $template->brand,
        "new"
    );
    $values_fb = array (
        $template->id,
        "in_stock",
        "new",
        $template->description,
        $template->img_fb,
        $template->link,
        $template->title,
        $template->price,
        $template->brand
    );
    $csvFile_google->addToFile($values_google);
    $csvFile_fb->addToFile($values_fb);
}
$csvFile_google->closeFile();
$csvFile_fb->closeFile();

