<?php

namespace prFeed\models;

use \Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'feed_table';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'title',
        'description',
        'price',
        'brand',
        'link',
        'img_fb',
        'img_google',
        'status',
    ];
}