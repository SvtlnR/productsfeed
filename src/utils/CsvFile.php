<?php
namespace prFeed\utils;
class CsvFile{
    private $filename;
    private $fp;
    function __construct($filename){
        $this->filename=$filename;
    }
    function openFile(){
        $this->fp = fopen($this->filename, 'w');
    }
    function  closeFile(){
        fclose($this->fp);
    }
    function addToFile($values){
        fputcsv($this->fp, $values);
    }

}