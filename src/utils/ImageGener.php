<?php
namespace prFeed\utils;
class ImageGener{
    private $img;
    public function __construct($filename){
        $this->img=imagecreatefromjpeg($filename);
    }
    function getWidth() {
        return imagesx($this->img);
    }
    function getHeight() {
        return imagesy($this->img);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function crop($width,$height){
        $this->img = imagecrop($this->img, ['x' => 0, 'y' => 0, 'width' => $width, 'height' => $height]);
    }
    function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->img, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->img = $new_image;
    }
    function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->img,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->img,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->img,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }
    function createImage($width,$height){
        $this->resizeToWidth($width);
        $this->crop($width,$height);
    }
}
